<?php

namespace OAuth2Podbbang\Server\Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

class Resource
{
    // Connects the routes in Silex
    public static function addRoutes($routing)
    {
        $routing->get('/resource', array(new self(), 'resource'))->bind('access');
    }

    /**
     * This is called by the client app once the client has obtained an access
     * token for the current user.  If the token is valid, the resource (in this
     * case, the "friends" of the current user) will be returned to the client
     */
    public function resource(Application $app)
    {
        // get the oauth server (configured in src/OAuth2Demo/Server/Server.php)
        $server = $app['oauth_server'];

        // get the oauth response (configured in src/OAuth2Demo/Server/Server.php)
        $response = $app['oauth_response'];

        if (!$server->verifyResourceRequest($app['request'], $response)) {//토큰 유효성 검증은 여기서 완료됨
            return $server->getResponse(); 
        } else {
            
            //회원 정보중 제공 가능한 기본값 (ex:이름,이메일, 연락처, 주소, 주소상세등등을 제공해주시면 됩니다.)
            $api_response = array(
                'account' => array(
                    'user_key'=>'test001', //required
                    'user_name'=>'테스트네임', 
                    'user_nick'=>'테스트닉네임',
                    'email_address'=>'ceo@demasian.com', //required
                    'user_level'=>1,
                    'updated'=>20180612,  //required
                    'result'=>1    //if error 0                
                )
            );
            return new Response(json_encode($api_response));
        }
    }
}
    