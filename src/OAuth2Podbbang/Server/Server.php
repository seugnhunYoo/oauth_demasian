<?php

namespace OAuth2Podbbang\Server;

use Silex\Application;
use Silex\ControllerProviderInterface;
use OAuth2\HttpFoundationBridge\Response as BridgeResponse;
use OAuth2\Server as OAuth2Server;
use OAuth2\Storage\Pdo;
use OAuth2\Storage\Memory;
use OAuth2\OpenID\GrantType\AuthorizationCode;
use OAuth2\GrantType\UserCredentials;
use OAuth2\GrantType\RefreshToken;

class Server implements ControllerProviderInterface
{
    /**
     * function to create the OAuth2 Server Object
     */
    public function setup(Application $app)
    {
        /** MYSQL                   
         * @see https://github.com/dsquier/oauth2-server-php-mysql
         * $db_build_sql=$storage->getBuildSql();         
         *
         * Register client:
         * <code>
         *  $dsn = 'mysql:host=localhost;dbname=mediacoop';
         *  $storage = new Pdo(array('dsn' => $dsn, 'username' => 'mediacoop', 'password' => 'testpassword'));
         * </code>
         **/



        /** REDIS         
         * To use, install "predis/predis" via composer
         *
         * Register client:
         * <code>
         *  $storage = new OAuth2\Storage\Redis($redis);
         *  $storage->setClientDetails($client_id, $client_secret, $redirect_uri);
         * </code>
         **/



        // 예제를 위해 sqlite를 사용함
        if (!file_exists($sqliteFile = __DIR__.'/../../../data/oauth.sqlite')) {
            include_once($this->getProjectRoot().'/data/rebuild_db.php');
        }

        $storage = new Pdo(array('dsn' => 'sqlite:'.$sqliteFile));
        //sqlite 여기까지 


        // create array of supported grant types
        $grantTypes = array(
            'authorization_code' => new AuthorizationCode($storage),
            'user_credentials'   => new UserCredentials($storage),
            'refresh_token'      => new RefreshToken($storage, array(
                'always_issue_new_refresh_token' => true,
            )),
        );

        // instantiate the oauth server
        $server = new OAuth2Server($storage, array(
            'enforce_state' => true,
            'allow_implicit' => true,
            'use_openid_connect' => false,
            'issuer' => $_SERVER['HTTP_HOST'],
        ),$grantTypes);

        // add the server to the silex "container" so we can use it in our controllers (see src/OAuth2Demo/Server/Controllers/.*)
        $app['oauth_server'] = $server;

        /**
         * add HttpFoundataionBridge Response to the container, which returns a silex-compatible response object
         * @see (https://github.com/bshaffer/oauth2-server-httpfoundation-bridge)
         */
        $app['oauth_response'] = new BridgeResponse();
    }

    /**
     * Connect the controller classes to the routes
     */
    public function connect(Application $app)
    {
        // create the oauth2 server object
        $this->setup($app);

        // creates a new controller based on the default route
        $routing = $app['controllers_factory'];

        /* Set corresponding endpoints on the controller classes */
        Controllers\Authorize::addRoutes($routing);
        Controllers\Token::addRoutes($routing);
        Controllers\Resource::addRoutes($routing);

        return $routing;
    }
}
